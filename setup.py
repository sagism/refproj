from setuptools import setup, find_packages


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='myproj',
    version='0.1',
    description="A small example package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    python_requires='>=3.7',
    packages=find_packages())
