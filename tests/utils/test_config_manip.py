#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import pytest
from pytest import fixture

from myproj.utils import config_manip as config


@fixture
def sample_file():
    '''
    Create a sample file with sample content
    '''
    sample_fname = "new_sample"
    content = "a=c"
    sample_fd = open(sample_fname, "w")
    sample_fd.write(content)
    sample_fd.close()
    yield sample_fname
    os.remove(sample_fname)


def create_file(text):
    sample_fname = "test_sample"
    sample_fd = open(sample_fname, "w")
    sample_fd.write(text)
    sample_fd.close()
    return sample_fname


def test_no_change_on_upsert_without_mods():
    # setup
    sample_filename = "sample.txt"
    original_content = "a=c"

    with open(sample_filename, 'w') as sample_fd:
        sample_fd.write(original_content)

    # execute
    config.upsert(sample_filename, "a", "c")

    # validate
    with open(sample_filename, 'r') as sample_fd:
        updated_content = sample_fd.read()

    assert updated_content == original_content

    # teardown
    os.remove(sample_filename)


def test_updates_single_variable_content():
    content = "a=c"
    fname = create_file(content)
    config.upsert(fname, "a", "z")
    updated_content = open(fname).read()
    assert updated_content == "a=z"
    os.remove(fname)  # won't happen if the test fails


def test_upsert_inserts_new_element():
    content = "a=c"
    fname = create_file(content)
    config.upsert(fname, "b", "z")
    updated_content = open(fname).read()
    assert "b=z" in updated_content
    os.remove(fname)  # won't happen if the test fails


def test_upsert_inserts_twice(sample_file):
    config.upsert(sample_file, "b", "z")
    config.upsert(sample_file, "x", "y")
    updated_content = open(sample_file).read()
    assert "b=z" in updated_content
    assert "x=y" in updated_content


def test_updating_non_existent_file_raises():
    with pytest.raises(FileNotFoundError):
        config.upsert("bbb", "b", "z")


@pytest.mark.parametrize(
    "content,key,value,expected",
    [
        ("a=c", "a", "z", "a=z"),
        ("a=c", "a", "c", "a=c"),
        ("", "a", "c", "a=c"),
    ],
)
def test_upsert(content, key, value, expected):
    # setup
    sample_filename = "sample.txt"

    with open(sample_filename, 'w') as sample_fd:
        sample_fd.write(content)

    # execute
    config.upsert(sample_filename, key, value)

    # validate
    with open(sample_filename, 'r') as sample_fd:
        updated_content = sample_fd.read()

    assert updated_content == expected

    # teardown
    os.remove(sample_filename)
