#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""File manipulation library testing
"""

import os

from pytest import fixture, mark

from myproj.utils.filemanip import replace


@fixture
def sample_file():
    """Create a sample file with sample content
    """
    sample_fname = "sample.txt"
    content = "line 1 line 1"
    sample_fd = open(sample_fname, "w")
    sample_fd.write(content)
    sample_fd.close()
    yield sample_fname
    os.remove(sample_fname)


@mark.slow
def test_replace__exchanges_the_first_line(sample_file):
    replace(sample_file, "line", "record")
    with open(sample_file, "r") as sample_fd:
        new_content = sample_fd.read()
    assert new_content.startswith('record'), \
        "expected record, got {}".format(new_content)


def test_replace__exchanges_two_occurences_in_same_line(sample_file):
    replace(sample_file, "line", "record")
    with open(sample_file, "r") as sample_fd:
        new_content = sample_fd.read()
    assert new_content == "record 1 record 1"


def test_replace__exchanges_two_occurences_on_different_lines():
    # create sample file
    sample_fname = "sample.txt"
    content = """line 1
line 2"""

    expected = """record 1
record 2"""

    with open(sample_fname, "w") as sample_fd:
        sample_fd.write(content)

    replace(sample_fname, "line", "record")

    with open(sample_fname, "r") as sample_fd:
        new_content = sample_fd.read()

    assert new_content == expected, "expected: \n{}\ngot:\n{}".format(
        repr(expected),
        repr(new_content))

    os.remove(sample_fname)


def test_replace__raises_when_files_does_not_exist():
    try:
        replace("randomfilename", "line", "record")
    except Exception:
        pass
    else:
        raise Exception("Expected replace to raise")


def test_replace__can_replace_regular_expression():
    sample_fname = "sample.txt"
    content = """Eurovision: 21/05/1988
Olympic games: 08/08/2020
The year now is 2020"""

    expected = """Eurovision: 21/05/2021
Olympic games: 08/08/2021
The year now is 2020"""

    with open(sample_fname, "w") as sample_fd:
        sample_fd.write(content)

    replace(sample_fname, r'(\d\d/\d\d)/(\d\d\d\d)', r'\1/2021')

    with open(sample_fname, "r") as sample_fd:
        new_content = sample_fd.read()

    assert new_content == expected

    os.remove(sample_fname)
