#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from myproj.utils import config_manip as config


def test_no_change_on_upsert_without_mods():
    # setup
    sample_filename = "sample.txt"
    original_content = "a=c"

    with open(sample_filename, 'w') as sample_fd:
        sample_fd.write(original_content)

    # execute
    config.upsert(sample_filename, "a", "c")

    # validate
    with open(sample_filename, 'r') as sample_fd:
        updated_content = sample_fd.read()

    if updated_content == original_content:
        # report
        print("test_no_change_on_upsert_without_mods: success")
    else:
        print("test_no_change_on_upsert_without_mods: error replacing text. expected \"{}\", got \"{}\"".format(
            original_content,
            updated_content
        ))

    # teardown
    os.remove(sample_filename)


def test_upsert_replaces_single_line():
    # setup
    sample_filename = "sample.txt"
    original_content = "a=c"
    expected_content = "a=z"

    with open(sample_filename, 'w') as sample_fd:
        sample_fd.write(original_content)

    # execute
    config.upsert(sample_filename, "a", "z")

    # validate
    with open(sample_filename, 'r') as sample_fd:
        updated_content = sample_fd.read()

    if updated_content == expected_content:
        # report
        print("test_upsert_replaces_single_line: success")
    else:
        print("test_upsert_replaces_single_line: error replacing text. expected \"{}\", got \"{}\"".format(
            original_content,
            updated_content
        ))

    # teardown
    os.remove(sample_filename)


def test_upsert_adds_new_element():
    pass


def test_upsert_twice():
    pass


def test_upsert_adds_to_empty_file():
    pass


def test_removes_duplicate_vars():
    pass


if __name__ == "__main__":
    # Run all the tests
    test_no_change_on_upsert_without_mods()
    test_upsert_replaces_single_line()
