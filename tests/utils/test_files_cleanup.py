from pathlib import Path

from myproj.utils.files_cleanup import cleanup


# patching using pytest's own monkeypatch
def test_includes_pyc_files(monkeypatch):
    def files(*args):
        # no that this primitive implementation of "glob()" returns this
        # list regardless of the passed params, so it is not a great
        # option, this could be implemented as a fixture and then it
        # could filter the items based on the pattern specified
        return map(Path, ['/abc.pyc', '/zzz.123'])

    monkeypatch.setattr(Path, "glob", files)
    kill_list = cleanup(Path('.'), dry_run=True)
    assert '/abc.pyc' in map(str, kill_list)
    assert '/zzz.123/' not in map(str, kill_list)

