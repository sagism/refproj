from behave import given, when, then

from myproj.rules import rule0
from myproj.rules import rule1


@given('current version is {version}')
def set_current_version(context, version):
    context.current_version = version


@given('latest version is {version}')
def set_latest_version(context, version):
    context.latest_version = version


@when('we run rule0')
def run_rule0(context):
    context.result = rule0.run()


@when('we run rule1')
def run_rule1(context):
    context.result = rule1.run()


@then('no upgrade was performed')
def check_no_upgrade(context):
    assert context.result is None
