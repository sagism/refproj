Feature: Rule 0 - Upgrade client

  Scenario: No upgrade when version is up to date
     Given current version is 1.2.3
     And latest version is 1.2.3
     When we run rule0
     Then no upgrade was performed
