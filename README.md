# Reference Python Project 
![refproj logo](./refproj.png)

This is a sample python project which can be used as a starter project. 

It is compliant with [the Python official packaging instructions](https://packaging.python.org/tutorials/packaging-projects/)

It suggests / includes the following toolchain:

  - `virtualenv` - To create a controlled python environment
  - `pip` - the standard python way to manage packages
  - `flake8` - linter
  - `pytest` - the leader of python all-purpose testing tools
  - `mock` - built into python 3 (in the form of `unittest.mock`), you will need to install it if you are still on python 2
  - `behave` - BDD testing package. Fully Gherkin compliant (as opposed to pytest.bdd)
  - `coverage` - Test coverage analysis and reporting
  - `Bandit` - Security vulnerabilities scanner
  - `pydoc` - built-in way to view documentation for python code

Notable alternatives and possible additions:

  - `pipenv` - combines virtualenv and pip into a more complete package management option. 
   Not standard, or even common yet, but promising
  - `poetry` - similar to pipenv. Another compelling package management alternative
  - `venv` - Built into python3, this is an alternative to virtualenv, but not yet on par
   with virtualenv
  - `Sphinx` (and others) - High-level documentation generation, goes way beyond the 
  functionality of pydoc
  - `Flake8` is definitely not the only game in town in the world of linter

## Installing

  - Copy / clone this package
  - install virtualenv and virtualenvwrapper
  - create the virtual environment: (specifying the path to the python interpreter, unless you want
  to use the default)

    `% mkvirtualenv -p /Library/Frameworks/Python.framework/Versions/3.7/bin/python3 refproj`
  - (I recommend adding a "cd" statement to this directory in the 
    `.../virtualenvs>/myproj/bin/postactivate` script
  so that when you switch to this virtual environment, you will automatically land into its directory
  - `pip install -r requirements.txt`

     Note that requirements.txt includes a line which says "`-e .`" - This line causes this project to be intalled
  in editable mode. The main desirable side effect is that we can import files from any of our packages
  without having to manually manipulate `sys.path` in our code. There are alternatives, such as adding the path
  to the root of our project to the site-packages. e.g.:

     `% pwd > $VIRTUAL_ENV/lib/pythonXXX/site-packages/myproj.pth` (replace XXX with your version of python)
    
## Customizing
If you intend to use this as a template to your own project. In that case, you will want to replace occurences of `myproj`, the top package, to your own top package.

You can do it like so (run at the project root dir): 

`% find . -type f -exec sed -i "" s/myproj/fuzzbar/g {} \;`

This replaces `myproj` to `fuzzbar`. It has been tested on a mac. I don't know how to do this on Windows, but at some point I will include a python script to do this.

## Operating

  - Test: 

    `% ./run_tests`
  - Run: 

    `% python myproj/rules/rule0.py` (for example)
  - Document: 

    `% pydoc myproj`

## Missing

  - Deployment - This is project specific
  - Git hook to run quality gates before commit

## Author
  - Sagi Smolarski - `sagism` at `gmail.com`

