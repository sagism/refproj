#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Rule 0 - A sample rule
"""
from myproj.utils.filemanip import replace


def run():
    dummy = 3
    if dummy > 0:
        pass
    return None


if __name__ == "__main__":
    run()
