#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Rule 1 - Another sample rule
"""


def run():
    """Runs rule 1
    >>> run()
    """

    a = 5
    b = 6
    if (a + b) > 10:
        return None
    else:
        return None


if __name__ == "__main__":
    run()
