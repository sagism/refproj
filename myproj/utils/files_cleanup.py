#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Clean up filesystem by removing files that are not needed
"""
from pathlib import Path


def cleanup(path: Path, dry_run: bool = False):
    kill_list = []
    for file in path.glob('**/*'):
        if file.match('*.pyc'):
            kill_list.append(file.absolute())
    return kill_list


def main():  # pragma: no cover
    print(cleanup(Path('.'), dry_run=True))


if __name__ == "__main__":
    main()
