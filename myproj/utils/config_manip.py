#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Utilities to manipulate environment variables in a file

Useful when you need to manipulate configuration in /etc/environment and other files
of the general structure:

env1=123
env2="my name"
...

"""

from io import StringIO
import re
import os


def upsert(fname, key, value):
    '''
    Update or insert an environment variable in a file
    :param fname: File name which contains the environment configuration we want to modify
    :param key: the name of the environment variable we want to create or modify
    :param value: the desired value of the environment variable
    :return: new buffer containing modified configuration

    Structure of the environment file should be something like:
    MY_ENV1=1234
    MY_ENV2="your name here"
    '''
    content = open(fname)
    new_content = upsert_buffer(content, key, value)
    tmpfile = fname + ".tmp"
    os.rename(fname, tmpfile)
    fd = open(fname, "w")
    fd.write(new_content.getvalue())
    os.remove(tmpfile)


def upsert_buffer(inbuffer: StringIO, key: str, value: str) -> StringIO:
    '''
    Update or insert an environment variable in a file-like buffer
    :param inbuffer: Contains the configuration we want to modify
    :param key: the name of the environment variable we want to create or modify
    :param value: the desired value of the environment variable
    :return: new buffer containing modified configuration

    Structure of the environment file should be something like:
    MY_ENV1=1234
    MY_ENV2="your name here"
    '''
    outbuffer = StringIO()
    inbuffer.seek(0)
    replaced = False
    for line in inbuffer.readlines():
        match = re.match(r'^\s*(' + key + r')\s*=(.*)(\n?)$', line)
        if match:
            groups = match.groups()
            outbuffer.write("{}={}{}".format(key, value, groups[2]))
            replaced = True
        else:
            outbuffer.write(line)

    if not replaced:
        outbuffer.write("{}={}".format(key, value))

    return outbuffer
