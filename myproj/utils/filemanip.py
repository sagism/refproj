import os
import re

def replace(fname: str, from_expr: str, to_expr: str):
    backup_fname = fname + ".bak"
    os.rename(fname, backup_fname)

    with open(backup_fname, 'r') as infile, open(fname, 'w+') as outfile:
        for line in infile:
            # outfile.write(line.replace(from_expr, to_expr))
            outfile.write(re.sub(from_expr, to_expr, line))
